﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Block
    {
        /* Block Variables */
        private DateTime timestamp; // Time of creation

        private int index, // Position of the block in the sequence of blocks
            difficulty = 4; // An arbitrary number of 0's to proceed a hash value

        public String prevHash, // A reference pointer to the previous block
            hash, // The current blocks "identity"
            merkleRoot,  // The merkle root of all transactions in the block
            minerAddress; // Public Key (Wallet Address) of the Miner

        public List<Transaction> transactionList; // List of transactions in this block

        // Proof-of-work
        public long nonce;
        public long nonce1,nonce2; // Number used once for Proof-of-Work and mining
        public String hash1 = String.Empty;
        public String hash2 = String.Empty;
        public bool thread1, thread2;
        // Rewards
        public double reward; // Simple fixed reward established by "Coinbase"

        /* Genesis block constructor */
        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            hash = Mine();
        }

        /* New Block constructor */
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress)
        {
            timestamp = DateTime.Now;

            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;

            this.minerAddress = minerAddress; // The wallet to be credited the reward for the mining effort
            reward = 1.0; // Assign a simple fixed value reward
            transactions.Add(createRewardTransaction(transactions)); // Create and append the reward transaction
            transactionList = new List<Transaction>(transactions); // Assign provided transactions to the block

            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions
            Stopwatch Timer = new Stopwatch();
            Timer.Start();
            hash = Mine();
            //hash = ThreadProc();
            Timer.Stop();
        }

        /* Hashes the entire Block object */
        public String CreateHash()
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);
            
            return hash;
        }
        public String CreateHash(long nonce)//Handling two nonce for our multithreading!
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }
        public String ThreadProc()
        {
            String difficulty_differ = new string('0', difficulty);
            Thread th1 = new Thread(Mine1);
            Thread th2 = new Thread(Mine2);

            th1.Start();
            th2.Start();

            while (th1.IsAlive == true || th2.IsAlive == true)
            {
                Thread.Sleep(1);
            }

            if (hash1.StartsWith(difficulty_differ) == true)
            {

                nonce = nonce1;
                return hash1;
            }
            else
            {
                nonce = nonce2;
                return hash2;
            }

        }

        public void Mine2(object obj)
        {
            thread2 = false;
            Boolean Checkinghash = false;
            String Hash;
            string diffuculty_difference = new string('0', this.difficulty);

            while (Checkinghash == false)
            {
                Hash = CreateHash(nonce1);
                if (Hash.StartsWith(diffuculty_difference) == true)
                {
                    Checkinghash = true;
                    hash2 = Hash;
                    thread2 = true;
                    return;
                }
                else if (thread2 == true)
                {
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    Checkinghash = false;
                    nonce2 += 2;
                }

            }
            return;
        }

        public void Mine1(object obj)
        {
            thread1 = false;
            Boolean Checkinghash = false;
            String Hash;
            string diffuculty_difference = new string('0', this.difficulty);

            while(Checkinghash == false)
            {
                Hash = CreateHash(nonce1);
                if (Hash.StartsWith(diffuculty_difference) == true)
                {
                    Checkinghash = true;
                    hash1 = Hash;
                    thread1 = true;
                    return;
                }
                else if (thread1 == true)
                {
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    Checkinghash = false;
                    nonce1 += 2;
                }

            }
            return;
        }

        // Create a Hash which satisfies the difficulty level required for PoW
        public String Mine()
        {
                nonce = 0; // Initalise the nonce
                String hash = CreateHash(); // Hash the block

                String re = new string('0', difficulty); // A string for analysing the PoW requirement

                while (!hash.StartsWith(re)) // Check the resultant hash against the "re" string
                {
                    nonce++; // Increment the nonce should the difficulty level not be satisfied
                    hash = CreateHash(); // Rehash with the new nonce as to generate a different hash
                }
              
            return hash; // Return the hash meeting the difficulty requirement
        }

        // Merkle Root Algorithm - Encodes transactions within a block into a single hash
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"
            
            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i=0; i<hashes.Count; i+=2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, ""); // Issue reward as a transaction in the new block
        }

        /* Concatenate all properties to output to the UI */
        public override string ToString()
        {
            return "[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timestamp
                + "\nPrevious Hash: " + prevHash
                + "\n-- PoW --"
                + "\nDifficulty Level: " + difficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n-- " + transactionList.Count + " Transactions --"
                +"\nMerkle Root: " + merkleRoot
                + "\n" + String.Join("\n", transactionList)
                + "\n[BLOCK END]";
        }
    }
}
